import React, { useEffect, useState } from "react";
import '../../assets/css/Home.css';
//components
import { Input } from "../elements/input/input";
import { BudgetCard } from "../elements/budgedCard/budgedCard";
import { ExpensesCard } from "../elements/expensesCard/expensesCard";
import { NewExpense } from "../elements/newExpense/newExpense";
//utils
import { expensesListOne } from "../../utils/dataExample"
export const Home = () => {
    const [amount, setAmount] = useState(150000);
    const [budgetSpent, setBudgetSpent] = useState(0);
    const [budgetRemaining, setBudgetRemaining] = useState(0);
    const [expensesList, setExpensesList] = useState(expensesListOne);
    const [update, setUpdate] = useState(false)

    useEffect(() => {
        setBudgetRemaining(budgetRemaining);
    },[budgetRemaining]);

    useEffect(() => {
        if(expensesList.length>0){
            setAmount(amount);
            setBudgetSpent(budgetSpent);
            setExpensesList(expensesList);
            handleBudgetSpent(expensesList);
            handleBudgetRemaining(amount, budgetSpent);
        }else{
            setBudgetSpent(0);
            handleBudgetRemaining(amount, budgetSpent);
        }
    },[expensesList,amount,budgetSpent,update]);

    const handleAmountChange = ( event ) => {
        const result = event.target.value.replace(/\D/g, '');
        setAmount(result);
    } 

    const handleBudgetSpent = (expensesList) => {
        let totalBudgetSpent = 0;
        expensesList.map((cards) => {
            const { amount } = cards;
            return(
                totalBudgetSpent = totalBudgetSpent + amount 
            );
        })
        setBudgetSpent(totalBudgetSpent)
    }

    const handleBudgetRemaining = (amount,budgetSpent) => {
        let totalBudgetRemaining = amount - budgetSpent;
        setBudgetRemaining(totalBudgetRemaining)
    }

    const handleExpensesList = (newList) => {
        setExpensesList(newList);
        setUpdate(!update)
    }

    return (
        <div className="home">   
            {/* Header */}
            <div className="home-header">
                <header className="header">
                    <h1> TRACK EXPENSES </h1>
                </header>
            </div>
            {/* Input budget amount */}
            <div className="home-input-amount">
                <Input
                    amount = {amount}
                    handleAmountChange = { handleAmountChange }
                />
            </div>
            {/* Budget & Expenses */}
            <div className="home-budget-and-expenses">
                <div className="home-column">
                    <h1 className="budget-and-expenses-title"> Budget </h1>
                    {/* Actual Budget */}
                    <BudgetCard
                        amount = {amount}
                        label = "Actual Budget"
                        color = "card card-1"
                        icon = ""
                    />
                    {/* Budget Spent */}
                    <BudgetCard
                        amount = {budgetSpent}
                        label = "Budget Spent"
                        color = "card card-2"
                        icon = ""
                    />
                    {/* Budget Remaining */}
                    <BudgetCard
                        amount = {budgetRemaining}
                        label = "Budget Remaining"
                        color = {budgetRemaining<0 ? "card card-4" : "card card-3"}
                        icon = ""
                    />
                </div>
                <div className="home-column">
                    <h1 className="budget-and-expenses-title"> Expenses </h1>
                    <ExpensesCard
                        expensesList = {expensesList}
                        handleExpensesList = {handleExpensesList}
                    />
                </div>
            </div>
            {/* New Expense */}
            <div className="home-new-expense">
                <div className="home-row">
                    <h1 className="budget-and-expenses-title-row"> New Expense </h1>
                    <NewExpense
                        handleExpensesList = {handleExpensesList}
                        expensesList = {expensesList}
                    />
                </div>
                
            </div>
        </div>
    );
}