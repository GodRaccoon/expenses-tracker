import React, {useState,useEffect} from "react";
import "./newExpense.css";

export const NewExpense = (props) => {
    const {handleExpensesList, expensesList} = props;
    const [expenseAmount, setExpenseAmount] = useState(0);
    const [expenseDescription, setExpenseDescription] = useState('');

    useEffect(() => {
        setExpenseAmount(expenseAmount);
        setExpenseDescription(expenseDescription);

    },[expenseAmount,expenseDescription]);


    const handleExpenseAmount = (event) => {
        const result = event.target.value.replace(/\D/g, '');
        setExpenseAmount(result);
    }

    const handleExpenseDescription = (event) => {
        const result = event.target.value;
        setExpenseDescription(result);
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        const newExpenseItem = {
            id: expensesList.length>0 ? expensesList[expensesList.length -1].id + 1 : 0,
            itemName: expenseDescription,
            amount: Number(expenseAmount),
            category: '',
            color: 'expenses-card expenses-card-' + Math.floor(Math.random() * (5 - 1 + 1) +1)
        }
        let newExpensesList = expensesList;
        newExpensesList.push(newExpenseItem);
        handleExpensesList(newExpensesList);
        setExpenseAmount(0);
        setExpenseDescription('');
    }

    return (
        <div className="new-expense-container">
            <div className="new-expense-cards">
                <div className="new-expense-card new-expense-card-1">
                    <form onSubmit={handleSubmit}>
                        <label htmlFor="input" className="input">
                            <input
                                style={{color:"white"}}
                                type="text"
                                placeholder="$ 0.00"
                                name="expenseAmount"
                                value={`${expenseAmount}`}
                                onChange={ (e) => handleExpenseAmount(e) }
                            />
                            <span className="label">Current budget</span>
                            <span className="focus-bg"></span>
                        </label>
                        <label htmlFor="input" className="input">
                            <input
                                name="expenseDescription"
                                style={{color:"white"}}
                                type="text"
                                placeholder="Description"
                                value={`${expenseDescription}`}
                                onChange={ (e) => handleExpenseDescription(e) }
                            />
                            <span className="label">Expense Description</span>
                            <span className="focus-bg"></span>
                        </label>
                        <input 
                            className="submit-new-expense" 
                            type="submit" />
                    </form>
                </div>
            </div>
        </div>
    );
}