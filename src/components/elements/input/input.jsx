import React from "react";
import "./input.css"

export const Input = (props) => {
    const { amount, handleAmountChange } = props;
    return (
        <div className="input-container">
            <label htmlFor="input" className="input">
                <input
                    style={{color:"white"}}
                    type="text"
                    placeholder="Type your actual budget"
                    value={`$ ${amount}`}
                    onChange={ (e) => handleAmountChange(e) }
                />
                <span className="label">Current budget</span>
                <span className="focus-bg"></span>
            </label>
        </div>
    );
}