import React from "react";
import "./expensesCard.css";

export const ExpensesCard = (props) => {
    const { expensesList, handleExpensesList } = props;

    const deleteCardExpenseItem = ( event ) => {
        const tempList = expensesList;
        const index = tempList.indexOf(event);
        tempList.splice(index,1);
        handleExpensesList(tempList);
    } 

    return (
        <div className="main-container-expenses-cards">
            <div className="expenses-cards">
                { expensesList.map((cards, index) => {
                        const { itemName, amount, color } = cards;
                        return(
                            <div key={index} className={color}>
                                <h2 className="expenses-card-title">${amount}</h2>
                                <p className="expenses-card-link">{itemName}</p>
                                <div className="expenses-card-title">
                                    <button className="button" onClick={()=>deleteCardExpenseItem(cards)}>
                                        <span>X</span>
                                    </button>
                                </div>
                            </div>
                        );
                    }
                )}
            </div>
        </div>
    );
}