import React from "react";
import "./budgedCard.css";

export const BudgetCard = (props) => {
    const { amount, label, color } = props;
    return (
        <div className="main-container">
            <div className="cards">
                <div className={color}>
                    <h2 className="card-title"><i className="fas fa-bolt"></i>${amount}</h2>
                    <p className="card-link">{label}</p>
                </div>
            </div>
        </div>
    );
}