export const expensesListOne = [
    {
        "id":0,
        "itemName":"Tacos",
        "amount":1150,
        "category":"food",
        "color":"expenses-card expenses-card-5"
    },
    {
        "id":1,
        "itemName":"Transport",
        "amount":2500,
        "category":"Transport",
        "color":"expenses-card expenses-card-4"
    },
    {
        "id":2,
        "itemName":"Car fixes",
        "amount":52500,
        "category":"Repairs",
        "color":"expenses-card expenses-card-3"
    },
    {
        "id":3,
        "itemName":"Internet Bill",
        "amount":8500,
        "category":"Bills",
        "color":"expenses-card expenses-card-2"
    }
];

export const expensesListTwo = [];

export const expensesListThree = [
    {
        "id":1,
        "itemName":"Vodka",
        "amount":3150,
        "category":"Alcohol",
        "color":"expenses-card expenses-card-5"
    },
    {
        "id":2,
        "itemName":"Nachos and Wings",
        "amount":5500,
        "category":"food",
        "color":"expenses-card expenses-card-4"
    },
    {
        "id":3,
        "itemName":"Uber",
        "amount":2500,
        "category":"Transport",
        "color":"expenses-card expenses-card-3"
    },
    {
        "id":4,
        "itemName":"Video Games",
        "amount":12500,
        "category":"Entertainment",
        "color":"expenses-card expenses-card-2"
    }
];